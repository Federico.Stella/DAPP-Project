#!/usr/bin/python3
# Copyright (c) 2015 Davide Gessa
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.
from libcontractvm import Wallet, WalletExplorer, ConsensusManager
from forum import ForumManager

import os
import sys
import time

consMan = ConsensusManager.ConsensusManager ()
consMan.bootstrap ("http://127.0.0.1:8181")

walletA = WalletExplorer.WalletExplorer (wallet_file='test.walletA')
AMan = ForumManager.ForumManager (consMan, wallet=walletA)

walletB = WalletExplorer.WalletExplorer (wallet_file='test.walletB')
BMan = ForumManager.ForumManager (consMan, wallet=walletB)


#post del messaggio di A
try:
	postIdA=AMan.createPost ('Hello post A', 'Post di test A')
	print ('POST-A >', postIdA)
except:
	print ('Errore')
time.sleep (5)


#creazione e visualizzazione della lista dei post con gli hash
def list():
	while 1:
		os.system ('clear')
		print ('Inizio lista di Post')
		lista = AMan.getList ()
		y=0
		for post in lista:
			y=y+1
			print (y, 'POST ->',post['hash'])
			try:
				if post['hash']==postid:
					return
			except:
				continue
		time.sleep (5)
list()
time.sleep (5)



#scrittura dei commenti da parte di A 
try:
	commid=AMan.createComment(postid, 'Commento A')
	print ('COMMENT-A >', commid)
except:
	print ('Errore')


time.sleep (5)

#recupero delle informazioni relative ai post precedenti
def getPostInfoA ():
	while True:
		os.system ('clear')
		print ('Info post')
		print ('Postid',)
		v=AMan.getListComments (postid)
		try:	
			if v['commidA']==commid:
				print ('POST(',v['hash'],')',v['title'],v['body'],'["',v['comment1'],'"]')
				return	
		except:
			print ('POST(',v['hash'],')',v['title'],v['body'])									
		time.sleep (5)
getPostInfoA()

time.sleep(10)



#post del messaggio di B
try:
	postIdB=BMan.createPost ('Hello post B', 'Post di test B')
	print ('POST-B >', postIdB)
except:
	print ('Errore')
time.sleep (5)


#recupero delle informazioni relative ai post precedenti
def getPostInfoB ():
	while True:
		os.system ('clear')
		print ('Info Post')

		print ('Post id : ',)
		v=BMan.getListcomments (postid)
		try:	
			if v['commidB']==commid2:
				print ('POST(',v['hash'],')',v['title'],v['body'],'["',v['comment1'],'" "',v['comment2'],'"]')
				return	
		except:
			print ('POST(',v['hash'],')',v['title'],v['body'],'["',v['comment1'],'"]')									
		time.sleep (5)

getPostInfoB()

#scrittura dei commenti da parte di B
try:
	commid2=BMan.createComment(postid, 'Commento B')
	print ('COMMENT-B >', commid2)
except:
	print ('Error')
time.sleep (5)




	
