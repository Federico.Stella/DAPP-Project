# Copyright (c) 2015 Davide Gessa
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

from libcontractvm import Wallet, ConsensusManager, DappManager

class ForumManager (DappManager.DappManager):
	def __init__ (self, consensusManager, wallet = None):
		super (ForumManager, self).__init__(consensusManager, wallet)

	def createPost (self, title, body):
		cid = self.produceTransaction ('forum.createPost', [title, body])
		return cid

	def createComment (self, postid, comment):
		cid = self.produceTransaction ('forum.createComment', [postid, comment])
		return cid

	def getList (self):
		return self.consensusManager.jsonConsensusCall ('forum.getList', [])['result']

	def getListComments (self, postid):
		return self.consensusManager.jsonConsensusCall ('forum.getListComments', [postid])['result']

	
