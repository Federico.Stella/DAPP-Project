# Copyright (c) 2015 Davide Gessa
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

import logging
import shelve #DATABASE

from contractvmd import dapp, config, proto
from contractvmd.chain import message

logger = logging.getLogger(config.APP_NAME)


class ForumProto:
	DAPP_CODE = [ 0x58, 0x60 ]
	METHOD_MES = 0x00
	METHOD_COM = 0X01
	METHOD_LIST = [METHOD_MES, METHOD_COM]

	
class ForumMessage (message.Message):
	def createPost (title, body):
		m = ForumMessage ()
		m.Title = title
		m.Body = body
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_MES
		return m


	def createComment (postid, comment):
		m = ForumMessage ()
		m.Postid = postid
		m.Comment = comment
		m.DappCode = ForumProto.DAPP_CODE
		m.Method = ForumProto.METHOD_COM
		return m

	def toJSON (self):
		data = super (ForumMessage, self).toJSON ()

		if self.Method == ForumProto.METHOD_MES:
			data['title'] = self.Title
			data['body'] = self.Body
		elif self.Method == ForumProto.METHOD_COM:
			data['postid'] = self.Postid
			data['comment'] = self.Comment
		else:
			return None

		return data



class ForumCore (dapp.Core):

	def __init__ (self, chain, database):
		database.init ('users', [])
		database.init ('comments', [])
		super (ForumCore, self).__init__ (chain, database)	
	
	#metodo per la creazione di post con hash, titolo e body	
	def createPost (self, hashs, title, body):
		self.database.listappend ('users', {'hash': hashs, 'title': title, 'body': body})

	#metodo per la creazione di commenti
	def createComment (self, hashs, postid, comment):
		self.database.listappend ('comments', {'hash': hashs, 'postid': postid, 'comment': comment})
	
	#metodo	 per recuperare la lista degli utenti	
	def getList (self):
		return self.database.get ('users')

	#def deletePost(self, comment)
	#	self.database.delete('comments')
		
	#metodo per scorrere la lista e recuperare dal db gli hash con i relativi commenti
	def getListComments (self, postid):
		users = self.database.get ('users')
		comments = self.database.get ('comments')
		
		for user in users:
			number=0
			if user['hash'] == postid:
				lis = user
				for idUser, lista in enumerate(b):
					if lista['postid'] == postid:
						number += 1
						lis['commid' + str(number)] = v['hash']
						lis['comment' + str(number)] = v['comment']
					else:
						continue
				return lis
			else:
				continue

		for user in users:
			if user['hash'] == postid:
				return user
			else:
				continue
		

class ForumAPI (dapp.API):
	def __init__ (self, core, dht, api):
		self.api = api
		rpcmethods = {}

		rpcmethods["getList"] = {
			"call": self.method_getlist,
			"help": {"args": [], "return": {}}
		}

		rpcmethods["getListComments"] = {
			"call": self.method_getlistcomments,
			"help": {"args": ["postid"], "return": {}}
		}

		rpcmethods["createPost"] = {
			"call": self.method_createPost,
			"help": {"args": ["title", "body"], "return": {}}
		}

		rpcmethods["createComment"] = {
			"call": self.method_createComment,
			"help": {"args": ["postid", "comment"], "return": {}}
		}

		errors = { }

		super (ForumAPI, self).__init__(core, dht, rpcmethods, errors)


	def method_getlist (self):
		return self.core.getList ()

	def method_getlistcomments (self, postid):
		return self.core.getListComments (postid)

	def method_createPost (self, title, body):
		msg = ForumMessage.createPost ( title, body)
		return self.createTransactionResponse (msg)

	def method_createComment (self, postid, comment):
		msg = ForumMessage.createComment ( postid, comment)
		return self.createTransactionResponse (msg)


class forum (dapp.Dapp):
	def __init__ (self, chain, db, dht, apiMaster):
		self.core = ForumCore (chain, db)
		apiprov = ForumAPI (self.core, dht, apiMaster)
		super (forum, self).__init__(ForumProto.DAPP_CODE, ForumProto.METHOD_LIST, chain, db, dht, apiprov)

	def handleMessage (self, m):
		if m.Method == ForumProto.METHOD_MES:
			logger.pluginfo ('Found new message %s:', m.Hash)
			self.core.createPost (m.Hash, m.Data['title'], m.Data['body'])	
		if m.Method == ForumProto.METHOD_COM:
			logger.pluginfo ('Found new comment %s:', m.Hash)
			self.core.createComment (m.Hash, m.Data['postid'], m.Data['comment'])			